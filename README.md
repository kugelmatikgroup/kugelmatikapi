# Kugelmatik API

Eine Schnittstelle um die Kugelmatik mithilfe verschiedener Programmierpsrachen zu steuern.
Die API ist KEINE komplette Rekreation der [Kugelmatik Libary](https://gitlab.com/kugelmatikgroup/kugelmatik-v2/-/tree/master/KugelmatikLibrary). 
Methoden und Klassen werden bereitgestellt um Befehle via TCP oder RFCOMM (nur Android) an den lokalen TCP/ RFCOMM Server zu schicken. Dieser Server wird beim Start der [Kugelmatik Control](https://gitlab.com/kugelmatikgroup/kugelmatik-v2/-/tree/master/KugelmatikControl) gestartet. 

|            | TCP | RFCOMM |
|------------|-----|--------|
| PyAPI      | ✔️   | ❌      |
| JavaAPI    | ✔️   | ❌      |
| CSAPI      | ✔️   | ❌      |
| AndroidAPI | ❌   | ✔️      |
