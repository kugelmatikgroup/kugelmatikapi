from typing import Union
from typing import List


class Coordinate:
    """
    Represents two pairs of Coordinates.
    One for the cluster and one for the stepper relative to the cluster.
    Coordinates are interpreted from 0-n
    """
    def __init__(self, cluster_x: int = -1, cluster_y: int = -1, sphere_x: int = -1, sphere_y: int = -1):
        self.cluster_x = cluster_x
        self.cluster_y = cluster_y
        self.sphere_x = sphere_x
        self.sphere_y = sphere_y

    def to_list(self) -> List[int]:
        return [self.cluster_x, self.cluster_y, self.sphere_x, self.sphere_y]


class DataSet:
    # TODO Union[int, Command] PosData
    def __init__(self, cmd: Union[int], coordinate: Coordinate, height: int):
        self.cmd = cmd
        self.coordinate = coordinate
        self.height = height

    def to_json_str(self) -> str:
        return "{" + \
               f"\"cmd\":{self.cmd}, " + \
               f"\"coordinate\":{self.coordinate.to_list()}, " + \
               f"\"heightValue\":{self.height}" + \
               "}"


class PosData:
    def __init__(self, coordinate: Coordinate, height: int = 0):
        self._coordinate = coordinate
        self._height = height

    @property
    def coordinate(self):
        return self._coordinate

    @property
    def height(self):
        return self._height
