import json
from typing import Union
from typing import List

from PyKugelmatikAPI.data import DataSet, PosData


class Command:
    SEND_HOME = 0
    DOWN = 1
    UP = 2
    STOP = 3
    SET = 4
    FIX = 5


class NetworkPackage:
    """
    Basic class to build a package with instructions for the Kugelmatik

    Parameter:
        cmd :type [Command, int], optional
            The command to execute.
        coordinate :type list(list(int)), optional
            The coordinates of the sphere where the command is executed.
        height :type int, optional
            The height value of the spheres to add or subtract

    Methods:
        get_json_str()
            :returns str with JSON content
        get_json()
            :returns dict if JSON is valid
    """

    def __init__(self, cmd: Union[Command, int], pos_data_list: List[PosData]):
        self.cmd = cmd
        self.pos_data_list = pos_data_list

    def get_json_str(self) -> str:
        data_sets_json_str = ""
        for index, pos_data in enumerate(self.pos_data_list):
            data_sets_json_str += DataSet(self.cmd, pos_data.coordinate, pos_data.height).to_json_str()
            if index < len(self.pos_data_list) - 1:
                data_sets_json_str += ", "
        return "{" + \
               f"\"dataSets\": [{data_sets_json_str}], \"clientInfo\":\"py\"" + \
               "}<EOP>"


def get_json(self) -> dict:
    try:
        return json.loads(self.get_json_str())
    except json.decoder.JSONDecodeError:
        print("ERROR: Content must be in JSON format.")
