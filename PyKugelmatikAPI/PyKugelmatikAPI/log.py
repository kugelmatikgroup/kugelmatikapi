import time
from datetime import datetime
import threading

class APILog:
    def __init__(self):
        # All time data
        self._packages_send = 0

        # Live data
        self._live_data_is_enabled = False
        self._packages_last_min = 0
        self._package_list = []

    def enable_live_data(self):
        self._live_data_is_enabled = True

    def disable_live_data(self):
        self._live_data_is_enabled = False

    def increase_packages_send(self) -> None:
        self._packages_send += 1
        if self._live_data_is_enabled:
            self._packages_last_min += 1
            threading.Thread(target=self._remove_package_after_min).start()

    def get_packages_send(self) -> int:
        # Since start of program
        return self._packages_send

    def get_packages_per_minute(self):
        return self._packages_last_min

    def reset(self):
        self._packages_send = 0

    def _remove_package_after_min(self):
        time.sleep(60)
        self._packages_last_min -= 1
