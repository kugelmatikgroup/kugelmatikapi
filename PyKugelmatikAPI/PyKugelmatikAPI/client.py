from PyKugelmatikAPI import network_package

import socket
from typing import Callable
from typing import Union
from threading import Thread

class Client:
    """
    The class represents a TCP connection to a Server.

    Parameter:
        auto_connect :type bool, optional
            If true the client trys to connect during the creation of a client object.

    Methods:
        send_data(bytes)
            Sends bytes to the connected server
        send_str(str)
            Sends a string to the Server.
        send_package(Package)
            Sends the JSON data of the package to the Server.
        close()
            Closes the connection.
            WARNING: Connection can't be opened again
    """

    def __init__(self, auto_connect=False):
        self.host = "localhost"
        self.port = 4840
        self.is_connected = False
        self.socket = None
        if auto_connect:
            self.connect()

    def send_data(self, data: bytes) -> str:
        """Sends bytes to the Server and returns the response"""
        if not self.is_connected:
            self.connect()
        self.socket.sendall(data)
        return self.socket.recv(256).decode("utf-8")
        # TODO erst buffer dann close?

    def send_str(self, data: str) -> str:
        """Sends a utf-8 string to the server and returns the response. The string is send as bytes"""
        return self.send_data(bytes(data, "utf-8"))

    def send_package(self, package_to_send: network_package.NetworkPackage) -> str:
        """Sends the json of a package as string bytes to the server and returns the response"""
        return self.send_str(package_to_send.get_json_str())

    def close(self):
        self.socket.close()
        self.is_connected = False

    def connect(self):
        """Connects to the server. WARNING: Only call this method if auto_connect is false"""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))
        self.is_connected = True

    def start_connect(self, callback: Callable[[bool, Union[ConnectionError, None]], ...]):
        def thread():
            try:
                self.connect()
                callback(True, None)
            except ConnectionError as e:
                callback(False, e)
        Thread(target=thread).start()