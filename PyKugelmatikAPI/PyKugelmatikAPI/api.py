from typing import Callable

from PyKugelmatikAPI.client import *
from PyKugelmatikAPI.network_package import *
from PyKugelmatikAPI.log import *
from PyKugelmatikAPI.data import *


class APIHandler:
    """
    A manager that holds all functions of the API.
    The manager contains a PyKugelmatikAPI.data.Client object.
    This object is the actual client connection to the API server.

    Parameter:
        auto_connect :type bool, optional
            If true the client tries to connect to the API server instantly
    """
    def __init__(self, auto_connect: bool = False):
        self._client = Client(auto_connect=auto_connect)
        self.api_log = APILog()

        self._event_callbacks = []

    @property
    def client(self):
        return self._client

    def connect(self):
        if not self._client.is_connected:
            self._client.connect()

    def start_connect(self, callback: Callable[[bool, Union[ConnectionError, None]], ...]):
        """
        Starts a new non blocking thread that tries to establish a connection to the server.
        WARNING: Only call this method if auto_connect is false

        Parameter:
            callback: Callable(bool, Union[ConnectionError, None])
                The callback is called either if the connection failed or could be established
                The boolean indicates the current state of connection (True=connected)
                The ConnectionError represents the error while connecting (None if connected successfully)
        """
        self._client.start_connect(callback)


    def on_package_send(self, callback: Callable[..., None]):
        self._event_callbacks.append(callback)

    def __fire_package_send(self):
        self.api_log.increase_packages_send()
        for callback in self._event_callbacks:
            callback()

    def set_spheres(self, pos_data_list: List[PosData]):
        """
        Sets spheres to a certain height
        :param pos_data_list: List[PosData]
            -> A list of position data where the coordinate and the matching height is stored.
        """
        self._client.send_package(NetworkPackage(cmd=Command.SET,
                                                 pos_data_list=pos_data_list
                                                 ))
        self.__fire_package_send()

    def move_up(self, pos_data_list: List[PosData]):
        """
        Moves spheres up by a certain number
        :param pos_data_list: List[PosData]
            -> A list of position data where the coordinate and the matching height is stored.
        """
        self._client.send_package(NetworkPackage(cmd=Command.UP,
                                                 pos_data_list=pos_data_list
                                                 ))
        self.__fire_package_send()

    def move_cluster_up(self, pos_data_list: List[PosData]):
        """
        Moves whole clusters up by a certain number
        :param pos_data_list: List[PosData]
            -> A list of position data where the coordinate and the matching height is stored.
        """
        for pos_data in pos_data_list:
            pos_data.coordinate.sphere_x = -1
            pos_data.coordinate.sphere_y = -1
        self._client.send_package(NetworkPackage(cmd=Command.UP,
                                                 pos_data_list=pos_data_list
                                                 ))
        self.__fire_package_send()

    def move_all_up(self, number: int):
        """
        Moves whole Kugelmatik up by a certain number
        :param number: int -> Defines how much the Spheres go up
        """
        self._client.send_package(NetworkPackage(cmd=Command.UP,
                                                 pos_data_list=[PosData(Coordinate(), height=number)]
                                                 ))
        self.__fire_package_send()

    def move_down(self, pos_data_list: List[PosData]):
        """
        Moves spheres down by a certain number
        :param pos_data_list: List[PosData]
            -> A list of position data where the coordinate and the matching height is stored.
        """
        self._client.send_package(NetworkPackage(cmd=Command.DOWN,
                                                 pos_data_list=pos_data_list))
        self.__fire_package_send()

    def move_cluster_down(self, pos_data_list: List[PosData]):
        """
        Moves whole clusters down by a certain number
        :param pos_data_list: List[PosData]
            -> A list of position data where the coordinate and the matching height is stored.
        """
        for pos_data in pos_data_list:
            pos_data.coordinate.sphere_x = -1
            pos_data.coordinate.sphere_y = -1
        self._client.send_package(NetworkPackage(cmd=Command.DOWN,
                                                 pos_data_list=pos_data_list
                                                 ))
        self.__fire_package_send()

    def move_all_down(self, number: int):
        """
        Moves whole Kugelmatik down by a certain number
        :param number: int -> Defines how much the Spheres go down
        """
        self._client.send_package(NetworkPackage(cmd=Command.DOWN,
                                                 pos_data_list=[PosData(Coordinate(), height=number)]
                                                 ))
        self.__fire_package_send()

    def send_home(self, pos_data_list: List[PosData]):
        """
        Sends specified clusters home.
        NOTE: Only whole clusters can be send home. NOT a single sphere.
        :param pos_data_list: List[PosData]
            -> A list of position data where the coordinate and the matching height is stored.
        """
        for pos_data in pos_data_list:
            pos_data.coordinate.sphere_x = -1
            pos_data.coordinate.sphere_y = -1
        self._client.send_package(NetworkPackage(cmd=Command.SEND_HOME,
                                                 pos_data_list=pos_data_list
                                                 ))
        self.__fire_package_send()

    def stop(self, pos_data_list: List[PosData]):
        """
        Stops specified clusters.
        NOTE: Only whole clusters can be stoped. NOT a single sphere.
        :param pos_data_list: List[PosData]
            -> A list of position data where the coordinate and the matching height is stored.
        """
        for pos_data in pos_data_list:
            pos_data.coordinate.sphere_x = -1
            pos_data.coordinate.sphere_y = -1
        self._client.send_package(NetworkPackage(cmd=Command.STOP,
                                                 pos_data_list=pos_data_list
                                                 ))
        self.__fire_package_send()

    def fix(self, pos_data_list: List[PosData]):
        """
        Fixes a certain Stepper
        :param pos_data_list: List[PosData]
            -> A list of position data where the coordinate of the stepper is stored.
        """
        self._client.send_package(NetworkPackage(cmd=Command.FIX,
                                                 pos_data_list=pos_data_list))
        self.__fire_package_send()

    def fix_cluster(self, pos_data_list: List[PosData]):
        """
        Fixes every stepper in cluster
        :param pos_data_list: List[PosData]
            -> A list of position data where the cluster coordinate is stored.
        """
        for pos_data in pos_data_list:
            pos_data.coordinate.sphere_x = -1
            pos_data.coordinate.sphere_y = -1
        self._client.send_package(NetworkPackage(cmd=Command.FIX,
                                                 pos_data_list=pos_data_list
                                                 ))
        self.__fire_package_send()

    def fix_all(self):
        """
        Fixes every stepper in the Kugelmatik
        """
        self._client.send_package(NetworkPackage(cmd=Command.FIX,
                                                 pos_data_list=[PosData(Coordinate())]
                                                 ))
        self.__fire_package_send()
