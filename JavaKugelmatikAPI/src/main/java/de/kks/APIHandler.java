package de.kks;

import de.kks.network.*;
import de.kks.network.Package;
import de.kks.network.exceptions.ConnectionException;

import java.util.List;

public class APIHandler {

    private IPClient ipClient;

    /**
     * WARNING: The API Server has to be closed before the program exits
     *
     * @throws ConnectionException for general connection troubles
     */
    public APIHandler() throws ConnectionException {
        init();
    }

    public void close() {
        ipClient.close();
    }

    private void init() throws ConnectionException {
        ipClient = new IPClient(false);
    }

    public void setSpheres(List<Coordinate> coordinates, int step) {
        ipClient.sendPackage(new Package(Command.SET, coordinates, step));
    }

    public void moveUp(List<Coordinate> coordinates, int number) {
        ipClient.sendPackage(new Package(Command.SPHERE_UP, coordinates, number));
    }

    public void moveClusterUp(List<Coordinate> cluster_coordinates, int number) {
        for (Coordinate coord : cluster_coordinates) {
            coord.setSphereX(-1);
            coord.setSphereY(-1);
            ipClient.sendPackage(new Package(Command.SPHERE_UP, List.of(coord), number));
        }
    }

    public void moveAllUp(int number) {
        ipClient.sendPackage(new Package(Command.SPHERE_UP, List.of(new Coordinate(-1, -1, -1, -1)), number));
    }

    public void moveDown(List<Coordinate> coordinates, int number) {
        ipClient.sendPackage(new Package(Command.SPHERE_DOWN, coordinates, number));
    }

    public void moveClusterDown(List<Coordinate> cluster_coordinates, int number) {
        for (Coordinate coord : cluster_coordinates) {
            coord.setSphereX(-1);
            coord.setSphereY(-1);
            ipClient.sendPackage(new Package(Command.SPHERE_DOWN, List.of(coord), number));
        }
    }

    public void moveAllDown(int number) {
        ipClient.sendPackage(new Package(Command.SPHERE_DOWN, List.of(new Coordinate(-1, -1, -1, -1)), number));
    }

    /**
     * @WARNING: Only whole clusters can be send home, NOT single Spheres.
     * @param cluster_coordinates the coordinates of clusters that should be sent home
     */
    public void sendHome(List<Coordinate> cluster_coordinates) {
        for (Coordinate coord : cluster_coordinates) {
            coord.setSphereX(-1);
            coord.setSphereY(-1);

            ipClient.sendPackage(new Package(Command.SEND_HOME, List.of(coord)));

        }
    }

    /**
     * WARNING: Only whole clusters can stop, NOT a single Sphere.
     *
     * @param cluster_coordinates the coordinates of clusters that should stop
     */
    public void stopCluster(List<Coordinate> cluster_coordinates) {

        for (Coordinate coord : cluster_coordinates) {
            coord.setSphereX(-1);
            coord.setSphereY(-1);

            ipClient.sendPackage(new Package(Command.STOP, List.of(coord)));

        }
    }

    public IPClient getIpClient() {
        return ipClient;
    }
}
