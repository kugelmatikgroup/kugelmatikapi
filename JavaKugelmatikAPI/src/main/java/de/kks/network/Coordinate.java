package de.kks.network;

public class Coordinate {
    private int clusterX;

    private int clusterY;

    private int sphereX;

    private int sphereY;

    public Coordinate() {
        this.clusterX = -1;
        this.clusterY = -1;
        this.sphereX = -1;
        this.sphereY = -1;
    }

    public Coordinate(int clusterX, int clusterY) {
        this.clusterX = clusterX;
        this.clusterY = clusterY;
        this.sphereX = -1;
        this.sphereY = -1;
    }

    public Coordinate(int clusterX, int clusterY, int sphereX, int sphereY) {
        this.clusterX = clusterX;
        this.clusterY = clusterY;
        this.sphereX = sphereX;
        this.sphereY = sphereY;
    }

    public String get_json_str() {
        return "[" +
                clusterX + ", " +
                clusterY + ", " +
                sphereX + ", " +
                sphereY +
                "]";
    }

    public int getClusterX() {
        return clusterX;
    }

    public void setClusterX(int clusterX) {
        this.clusterX = clusterX;
    }

    public int getClusterY() {
        return clusterY;
    }

    public void setClusterY(int clusterY) {
        this.clusterY = clusterY;
    }

    public int getSphereX() {
        return sphereX;
    }

    public void setSphereX(int sphereX) {
        this.sphereX = sphereX;
    }

    public int getSphereY() {
        return sphereY;
    }

    public void setSphereY(int sphereY) {
        this.sphereY = sphereY;
    }
}
