package de.kks.network;

import de.kks.network.exceptions.ConnectionException;

import java.io.*;
import java.net.Socket;

public class IPClient {
    private Socket socket;
    private DataOutputStream out;
    private DataInputStream in;

    private String host = "127.0.0.1";

    private int port = 4840;

    private boolean isConnected = false;

    public IPClient(boolean autoConnect) throws ConnectionException {
        if (autoConnect) {
            connect();
        }
    }

    public IPClient(String host, int port, boolean autoConnect) throws ConnectionException {
        this.host = host;
        this.port = port;

        if (autoConnect) {
            connect();
        }
    }

    public void connect() throws ConnectionException {
        try {
            socket = new Socket(host, port);
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());
            isConnected = true;
        } catch (IOException e) {
            throw new ConnectionException("Cant connect to Server!");
        }
    }

    public void close() {
        try {
            out.close();
            in.close();
            socket.close();
            isConnected = false;
        } catch (IOException e) {
            // Ignored
        }
    }

    public void sendData(String data) {
        if (!isConnected) {
            try {
                connect();
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        }
        try {
            out.writeUTF(data);
            // TODO response // type should be char[256]
            in.readUTF();
            //System.out.println(Character.toString(in.read()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendPackage(Package packageToSend) {
        sendData(packageToSend.get_json_str());
    }

    public boolean isConnected() {
        return this.isConnected;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
