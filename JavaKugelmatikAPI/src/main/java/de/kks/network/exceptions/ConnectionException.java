package de.kks.network.exceptions;

public class ConnectionException extends Exception {
    public ConnectionException() {

    }

    public ConnectionException(String msg) {
        super("Error while connecting to Server. " + msg);
    }
}
