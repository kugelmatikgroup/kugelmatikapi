package de.kks.network;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

public class Package {
    private final Command cmd;

    private final List<Coordinate> coordinates;

    int step = 0;

    public Package(Command cmd, List<Coordinate> coordinates) {
        this.cmd = cmd;
        this.coordinates = coordinates;
    }

    public Package(Command cmd, List<Coordinate> coordinates, int step) {
        this.cmd = cmd;
        this.coordinates = coordinates;
        this.step = step;
    }

    public String get_json_str() {
        StringBuilder builder = new StringBuilder("{");
        builder.append("\"cmd\":").append(this.cmd.getId()).append(", ");
        builder.append("\"coordinates\":[");
        Iterator<Coordinate> iterator = coordinates.iterator();
        while (iterator.hasNext()) {
            Coordinate coord = iterator.next();
            builder.append(coord.get_json_str());
            if (iterator.hasNext()) {
                builder.append(", ");
            }
        }
        builder.append("], ");
        builder.append("\"step\":").append(this.step);
        builder.append("}<EOP>");

        return builder.toString();
    }

    public JSONObject get_json() {
        try {
            return new JSONObject(this.get_json_str());
        } catch (JSONException e) {
            System.out.println("ERROR: Content must be in JSON format.");
        }
        return null;
    }

    public Command getCmd() {
        return cmd;
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }
}