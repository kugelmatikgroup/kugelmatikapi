package de.kks.androidapi.network;

public class DataSet {
    private Command cmd = Command.UNDEFINED;
    private Coordinate coordinate = Coordinate.WHOLE_KUGELMATIK();
    private short height = 0;

    public DataSet() {
    }

    public DataSet(PosData posData) {
        this.coordinate = posData.getCoordinate();
        this.height = posData.getHeightValue();
    }

    public DataSet(Command cmd, PosData posData) {
        this.cmd = cmd;
        this.coordinate = posData.getCoordinate();
        this.height = posData.getHeightValue();
    }

    public DataSet(Coordinate coordinate, short height) {
        this.coordinate = coordinate;
        this.height = height;
    }

    public DataSet(Command cmd, Coordinate coordinate, short height) {
        this.cmd = cmd;
        this.coordinate = coordinate;
        this.height = height;
    }

    public String toJsonString() {
        return "{" +
                "\"cmd\":" + cmd.getId() + ", " +
                "\"coordinate\":" + coordinate.toJsonString() + ", " +
                "\"heightValue\":" + height +
                "}";
    }

    public Command getCmd() {
        return cmd;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public short getHeight() {
        return height;
    }

    public void setCmd(Command cmd) {
        this.cmd = cmd;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public void setHeight(short height) {
        this.height = height;
    }
}
