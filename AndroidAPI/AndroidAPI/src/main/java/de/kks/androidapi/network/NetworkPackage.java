package de.kks.androidapi.network;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

public class NetworkPackage {
    private final List<DataSet> dataSets;
    private String clientInfo = "android";


    public NetworkPackage(List<DataSet> dataSets, String clientInfo) {
        this.dataSets = dataSets;
        this.clientInfo = clientInfo;
    }

    public NetworkPackage(List<DataSet> dataSets) {
        this.dataSets = dataSets;
    }

    public String get_json_str() {
        StringBuilder builder = new StringBuilder("{");
        builder.append("\"dataSets\":[");
        Iterator<DataSet> iterator = dataSets.iterator();
        while (iterator.hasNext()) {
            DataSet dataSet = iterator.next();
            builder.append(dataSet.toJsonString());
            if (iterator.hasNext()) {
                builder.append(", ");
            }
        }
        builder.append("], ");
        builder.append("\"clientInfo\":\"");
        builder.append(clientInfo);
        builder.append("\"}<EOP>");
        return builder.toString();
    }

    public JSONObject get_json() {
        try {
            return new JSONObject(this.get_json_str());
        } catch (JSONException e) {
            System.out.println("ERROR: Content must be in JSON format.");
        }
        return null;
    }

    public List<DataSet> getDataSets() {
        return dataSets;
    }

    public String getClientInfo() {
        return clientInfo;
    }
}