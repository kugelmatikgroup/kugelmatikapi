package de.kks.androidapi.network.exceptions;

public class BluetoothNotSupportedException extends Exception {
    public BluetoothNotSupportedException() {
        super("Bluetooth isn't supported!");
    }

    public BluetoothNotSupportedException(String extraMsg) {
        super("Bluetooth isn't supported! " + extraMsg);
    }
}
