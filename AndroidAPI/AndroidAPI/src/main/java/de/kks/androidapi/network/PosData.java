package de.kks.androidapi.network;

public class PosData {
    private final Coordinate coordinate;
    private short heightValue = 0;

    /**
     * In this constructor the heightValue will be set to 0
     * @param coordinate a coordinate on the Kugelmatik
     */
    public PosData(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    /**
     *
     * @param coordinate a coordinate on the Kugelmatik
     * @param heightValue a heigth value
     */
    public PosData(Coordinate coordinate, short heightValue) {
        this.coordinate = coordinate;
        this.heightValue = heightValue;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public short getHeightValue() {
        return heightValue;
    }
}
