package de.kks.androidapi.network;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import androidx.annotation.RequiresPermission;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.UUID;

import de.kks.androidapi.network.events.OnBondListener;
import de.kks.androidapi.network.events.OnConnectListener;
import de.kks.androidapi.network.exceptions.BluetoothDisableException;
import de.kks.androidapi.network.exceptions.BluetoothNotSupportedException;
import de.kks.androidapi.network.exceptions.ConnectionException;

public class BTClient {

    // btinfo https://bluetoothinstaller.com/bluetooth-command-line-tools/btinfo.html
    private String host = "5C:F3:70:98:43:D5";
    private UUID serviceID = UUID.fromString("e0cbf06c-cd8b-4647-bb8a-263b43f0f974");
    private BluetoothDevice server;

    private BluetoothSocket socket;
    private OutputStream out;
    private BufferedReader in;

    private BluetoothAdapter bluetoothAdapter;

    private boolean isConnected = false;
    private boolean isConnecting = false;

    // Is used to register a broadcast receiver and run background tasks
    private final Activity activity;

    // Listener
    OnConnectListener connectListener;
    OnBondListener bondListener;

    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    public BTClient(Activity activity) {
        this.activity = activity;
        init();
    }

    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    private void init() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        activity.getApplicationContext().registerReceiver(new BroadcastReceiver() {
            @Override
            @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
            public void onReceive(Context context, Intent intent) {
                int bondState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1);
                if (bondState == BluetoothDevice.BOND_BONDED) {
                    onBonded();
                } else if (bondState == BluetoothDevice.BOND_NONE) {
                    onBondFailed();
                }
            }
        }, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
    }

    // If the device is paired with the Server it will create a RFCOM socket
    // otherwise it will try to pair with the server device.
    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    public void connect(OnBondListener bondListener, OnConnectListener connectListener) throws BluetoothNotSupportedException, BluetoothDisableException, IllegalArgumentException {
        Log.d("CONNECT", "Start connect procedure...");
        if (bluetoothAdapter == null) {
            throw new BluetoothNotSupportedException();
        }
        if (!bluetoothAdapter.isEnabled()) {
            throw new BluetoothDisableException();
        }

        if (!isConnecting) {
            server = findServer();
            new Thread(() -> {
                isConnecting = true;
                this.bondListener = bondListener;
                this.connectListener = connectListener;


                if (isPairedWithServer()) {
                    startRFCOM();
                } else {
                    pair();
                }
                isConnecting = false;
            }).start();
        } else {
            Log.e("connect", "Error: Client is currently connecting!");
        }
    }

    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    public void startRFCOM() {
        Log.d("RFCOM", "Start RFCOM connection...");
        try {
            socket = server.createRfcommSocketToServiceRecord(serviceID);
            socket.connect();
            out = socket.getOutputStream();
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            isConnected = true;
            activity.runOnUiThread(() -> connectListener.onConnect());
        } catch (IOException e) {
            e.printStackTrace();
            activity.runOnUiThread(() -> connectListener.onConnectError());
        }
    }

    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    public void pair() {
        Log.d("PAIR", "Start pairing...");
        activity.runOnUiThread(() -> bondListener.onBondStart());
        if (!server.createBond()) {
            activity.runOnUiThread(() -> bondListener.onBondFailed());
        }
    }

    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    private void onBonded() {
        Log.d("PAIR_SUCSESS", "Pairing sucsessfull!");
        startRFCOM();
    }

    private void onBondFailed() {
        Log.d("PAIR_FAILED", "Bond failed!");
        activity.runOnUiThread(() -> bondListener.onBondFailed());
    }

    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    public boolean isPairedWithServer() {
        if (server == null) {
            return false;
        }
        return server.getBondState() == BluetoothDevice.BOND_BONDED;
    }

    /**
     * @return The found device for the BTClient.host MAC adress.
     * @throws IllegalArgumentException If host isn't a valid BT adress
     */
    private BluetoothDevice findServer() throws IllegalArgumentException {
        return bluetoothAdapter.getRemoteDevice(host);
    }

    public void close() {
        try {
            in.close();
            out.close();
            socket.close();
            isConnected = false;
        } catch (IOException e) {
            // Ignored
        } catch (NullPointerException e) {
            // No connection to Server -> invalid Objects
        }
    }

    public void sendData(String data) {
        if (isConnected) {
            new Thread(() -> {
                try {
                    out.write(data.getBytes());
                    out.flush();

                    // TODO Server response
                    in.readLine();
                } catch (IOException | NullPointerException e) {
                    isConnected = false;
                }
            }).start();

        } else {
            new ConnectionException("Client isn't connected to Server!").printStackTrace();
        }
    }

    public void sendPackage(NetworkPackage packageToSend) {
        sendData(packageToSend.get_json_str());
    }

    /**
     * @param host A String representing the MAC address of the target Bluetooth Server
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @param uuid A String representing a service ID
     * @throws IllegalArgumentException Is thrown if the String isn't a valid UUID.
     */
    public void setServiceID(String uuid) throws IllegalArgumentException {
        this.serviceID = UUID.fromString(uuid);
    }


    public boolean isConnected() {
        return this.isConnected;
    }

    public String getHost() {
        return host;
    }

    public UUID getServiceID() {
        return serviceID;
    }

    public BluetoothDevice getServer() {
        return server;
    }
}
