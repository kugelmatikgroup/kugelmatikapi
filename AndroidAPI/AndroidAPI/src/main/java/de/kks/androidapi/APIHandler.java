package de.kks.androidapi;

import android.Manifest;
import android.app.Activity;

import androidx.annotation.RequiresPermission;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.kks.androidapi.network.BTClient;
import de.kks.androidapi.network.Command;
import de.kks.androidapi.network.Coordinate;
import de.kks.androidapi.network.DataSet;
import de.kks.androidapi.network.NetworkPackage;
import de.kks.androidapi.network.PosData;
import de.kks.androidapi.network.events.OnBondListener;
import de.kks.androidapi.network.events.OnConnectListener;
import de.kks.androidapi.network.exceptions.BluetoothDisableException;
import de.kks.androidapi.network.exceptions.BluetoothNotSupportedException;

public class APIHandler {

    private BTClient btClient;

    private final Activity activity;

    /**
     * WARNING: Call APIHandler.connect() before executing commands
     * WARNING: The API Server has to be closed before the program exits
     */
    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    public APIHandler(Activity activity) {
        this.activity = activity;
        init();
    }

    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    private void init() {
        // Create new Bluetooth Client object.
        btClient = new BTClient(activity);
    }

    /**
     * Starts a connect procedure to create a RFCOM Socket with the Server PC
     * @param bondListener onBond is called when the app successfully paired with the Server PC.
     *                     onBondStart is called when the app starts the pairing procedure.
     *                     onBondFailed is called when the pairing is not successfull (often caused by: Bluetooth off on Server PC).
     * @param connectListener onConnect is called when the server successfully created a RFCOM socket.
     *                        onConnectError is called when during this Process is an error (often caused by: Server offline).
     *
     * @throws BluetoothNotSupportedException if the device does not support Bluetooth
     * @throws BluetoothDisableException if bluetooth is disabled
     */
    @RequiresPermission(Manifest.permission.BLUETOOTH_CONNECT)
    public void connect(OnBondListener bondListener, OnConnectListener connectListener) throws BluetoothNotSupportedException, BluetoothDisableException {
        btClient.connect(bondListener, connectListener);
    }

    /**
     * Closes all Streams and the Socket
     * Call this before the program exits
     */
    public void close() {
        btClient.close();
    }

    /**
     * Sets spheres to a certain height
     * @param posDataList A list of position data where the coordinate and the matching height is stored.
     */
    public void setSpheres(List<PosData> posDataList) {
        List<DataSet> dataSets = new ArrayList<>();
        for (PosData posData : posDataList) {
            dataSets.add(new DataSet(Command.SET, posData));
        }
        btClient.sendPackage(new NetworkPackage(dataSets));
    }

    /**
     * Moves spheres up by a certain number
     * @param posDataList A list of position data where the coordinate and the matching height is stored.
     */
    public void moveUp(List<PosData> posDataList) {
        List<DataSet> dataSets = new ArrayList<>();
        for (PosData posData : posDataList) {
            dataSets.add(new DataSet(Command.SPHERE_UP, posData));
        }
        btClient.sendPackage(new NetworkPackage(dataSets));
    }

    /**
     * Moves whole clusters up by a certain number
     * @param posDataList A list of position data where the coordinate and the matching height is stored.
     */
    public void moveClusterUp(List<PosData> posDataList) {
        List<DataSet> dataSets = new ArrayList<>();
        for (PosData posData : posDataList) {
            posData.getCoordinate().setSphereX(-1);
            posData.getCoordinate().setSphereY(-1);
            dataSets.add(new DataSet(Command.SPHERE_UP, posData));
        }
        btClient.sendPackage(new NetworkPackage(dataSets));
    }

    /**
     * Moves whole Kugelmatik up by a certain number
     * @param number Defines how much the Spheres go up
     */
    public void moveAllUp(short number) {
        btClient.sendPackage(new NetworkPackage(Collections.singletonList(new DataSet(
                Command.SPHERE_UP,
                new PosData(Coordinate.WHOLE_KUGELMATIK(), number)
        ))));
    }

    /**
     * Moves spheres down by a certain number
     * @param posDataList A list of position data where the coordinate and the matching height is stored.
     */
    public void moveDown(List<PosData> posDataList) {
        List<DataSet> dataSets = new ArrayList<>();
        for (PosData posData : posDataList) {
            dataSets.add(new DataSet(Command.SPHERE_DOWN, posData));
        }
        btClient.sendPackage(new NetworkPackage(dataSets));
    }

    /**
     * Moves whole clusters down by a certain number
     * @param posDataList A list of position data where the coordinate and the matching height is stored.
     */
    public void moveClusterDown(List<PosData> posDataList) {
        List<DataSet> dataSets = new ArrayList<>();
        for (PosData posData : posDataList) {
            posData.getCoordinate().setSphereX(-1);
            posData.getCoordinate().setSphereY(-1);
            dataSets.add(new DataSet(Command.SPHERE_DOWN, posData));
        }
        btClient.sendPackage(new NetworkPackage(dataSets));
    }

    /**
     * Moves whole Kugelmatik down by a certain number
     * @param number Defines how much the Spheres go down
     */
    public void moveAllDown(short number) {
        btClient.sendPackage(new NetworkPackage(Collections.singletonList(new DataSet(
                Command.SPHERE_UP,
                new PosData(Coordinate.WHOLE_KUGELMATIK(), number)
        ))));
    }

    /**
     * Sends specified clusters home.
     * NOTE: Only whole clusters can be send home. NOT a single sphere.
     * @param posDataList A list of position data where the coordinate and the matching height is stored.
     */
    public void sendHome(List<PosData> posDataList) {
        // WARNING: Only whole clusters can be send home NOT single Spheres.
        List<DataSet> dataSets = new ArrayList<>();
        for (PosData posData : posDataList) {
            posData.getCoordinate().setSphereX(-1);
            posData.getCoordinate().setSphereY(-1);
            dataSets.add(new DataSet(Command.SEND_HOME, posData));
        }
        btClient.sendPackage(new NetworkPackage(dataSets));
    }

    /**
     * Stops specified clusters.
     * NOTE: Only whole clusters can be stoped. NOT a single sphere.
     * @param posDataList A list of position data where the coordinate and the matching height is stored.
     */
    public void stopCluster(List<PosData> posDataList) {
        List<DataSet> dataSets = new ArrayList<>();
        for (PosData posData : posDataList) {
            posData.getCoordinate().setSphereX(-1);
            posData.getCoordinate().setSphereY(-1);
            dataSets.add(new DataSet(Command.STOP, posData));
        }
        btClient.sendPackage(new NetworkPackage(dataSets));
    }

    /**
     * Fixes a certain Stepper
     * @param posDataList A list of position data where the coordinate of the stepper is stored.
     */
    public void fix(List<PosData> posDataList) {
        List<DataSet> dataSets = new ArrayList<>();
        for (PosData posData : posDataList) {
            dataSets.add(new DataSet(Command.FIX, posData));
        }
        btClient.sendPackage(new NetworkPackage(dataSets));
    }

    /**
     * Fixes every stepper in cluster
     * @param posDataList A list of position data where the cluster coordinate is stored.
     */
    public void fixCluster(List<PosData> posDataList) {
        List<DataSet> dataSets = new ArrayList<>();
        for (PosData posData : posDataList) {
            posData.getCoordinate().setSphereX(-1);
            posData.getCoordinate().setSphereY(-1);
            dataSets.add(new DataSet(Command.FIX, posData));
        }
        btClient.sendPackage(new NetworkPackage(dataSets));
    }

    /**
     * Fixes every stepper in the Kugelmatik
     */
    public void fixAll() {
        btClient.sendPackage(new NetworkPackage(Collections.singletonList(new DataSet(
                Command.FIX,
                new PosData(Coordinate.WHOLE_KUGELMATIK())
        ))));
    }

    public BTClient getBtClient() {
        return btClient;
    }
}
