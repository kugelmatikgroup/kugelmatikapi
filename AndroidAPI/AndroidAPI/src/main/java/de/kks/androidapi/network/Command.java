package de.kks.androidapi.network;


public enum Command {
    UNDEFINED(-1),
    SEND_HOME(0),
    SPHERE_DOWN(1),
    SPHERE_UP(2),
    STOP(3),
    SET(4),
    FIX(5);

    private final int id;

    public int getId() {
        return this.id;
    }

    Command(int id) {
        this.id = id;
    }
}