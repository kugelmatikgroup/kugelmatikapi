package de.kks.androidapi.network.events;

public interface OnConnectListener {
    void onConnect();

    void onConnectError();
}
