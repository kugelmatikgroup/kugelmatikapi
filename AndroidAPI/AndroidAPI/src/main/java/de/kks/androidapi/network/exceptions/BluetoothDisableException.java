package de.kks.androidapi.network.exceptions;

public class BluetoothDisableException extends Exception {
    public BluetoothDisableException() {
        super("Bluetooth is not enabled!");

    }

    public BluetoothDisableException(String extraMsg) {
        super("Bluetooth is not enabled! " + extraMsg);
    }

}
