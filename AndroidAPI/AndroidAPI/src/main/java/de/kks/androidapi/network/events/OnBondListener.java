package de.kks.androidapi.network.events;

public interface OnBondListener {
    void onBond();

    void onBondStart();

    void onBondFailed();
}
