package de.kks.androidapi.network;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

public class Package {
    private final Command cmd;

    private final List<Coordinate> coordinates;

    int height = 0;


    public Package(Command cmd, List<Coordinate> coordinates) {
        this.cmd = cmd;
        this.coordinates = coordinates;
    }

    public Package(Command cmd, List<Coordinate> coordinates, int height) {
        this.cmd = cmd;
        this.coordinates = coordinates;
        this.height = height;
    }

    public String get_json_str() {
        StringBuilder builder = new StringBuilder("{");
        builder.append("\"cmd\":").append(this.cmd.getId()).append(", ");
        builder.append("\"coordinates\":[");
        Iterator<Coordinate> iterator = coordinates.iterator();
        while (iterator.hasNext()) {
            Coordinate coord = iterator.next();
            builder.append(coord.toJsonString());
            if (iterator.hasNext()) {
                builder.append(", ");
            }
        }
        builder.append("], ");
        builder.append("\"height\":").append(this.height);
        builder.append("}<EOP>");

        return builder.toString();
    }

    public JSONObject get_json() {
        try {
            return new JSONObject(this.get_json_str());
        } catch (JSONException e) {
            System.out.println("ERROR: Content must be in JSON format.");
        }
        return null;
    }

    public Command getCmd() {
        return cmd;
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}