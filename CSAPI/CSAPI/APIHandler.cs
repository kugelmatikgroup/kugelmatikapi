﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using CSAPI.network;

namespace CSAPI
{
    public class APIHandler
    {
        private Client client;
        public event EventHandler<ConnectEventArgs> connectEvent;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="autoConnect">If false APIHandler.Connect() has to be called manually</param>
        public APIHandler(bool autoConnect)
        {
            Init();
            if (autoConnect)
            {
                client.Connect();
            }
        }

        private void Init()
        {
            client = new Client();
        }

        /// <summary>
        /// Tries to establish a connection to the host.
        /// Only call this if autoConnect is false.
        /// </summary>
        public void Connect()
        {
            if (!client.IsConnected())
                client.Connect();
        }

        /// <summary>
        /// Tries to establish a connection to the host in a new Thread.
        /// APIHandler.connectEvent is called. It contains information about the current connection state.
        /// Only call this if autoConnect is false.
        /// </summary>
        public void StartConnect()
        {
            new Thread(() =>
            {
                try
                {
                    Connect();
                    connectEvent?.Invoke(null, new ConnectEventArgs(true, null));
                } catch (SocketException e)
                {
                    connectEvent?.Invoke(null, new ConnectEventArgs(false, e));
                }  
            }).Start();
        }

        /// <summary>
        /// Closes the current TCP connection and all Network Streams.
        /// Call this before the program exits.
        /// </summary>
        public void Disconnect()
        {
            if (client.IsConnected())
                client.Disconnect();
        }

        /// <summary>
        /// Sets spheres to a certain height
        /// </summary>
        /// <param name="posData">A list of position data where the coordinate and the matching height is stored</param>
        public void SetSpheres(List<PosData> posData)
        {
            List<DataSet> dataSets = new List<DataSet>();
            foreach (PosData data in posData)
            {
                dataSets.Add(new DataSet(Command.SET, data));
            }
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Moves spheres up by a certain number
        /// </summary>
        /// <param name="posData">A list of position data where the coordinate and the matching height is stored</param>
        public void MoveUp(List<PosData> posData)
        {
            List<DataSet> dataSets = new List<DataSet>();
            foreach (PosData data in posData)
            {
                dataSets.Add(new DataSet(Command.SPHERE_UP, data));
            }
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Moves whole clusters up by a certain number
        /// </summary>
        /// <param name="posData">A list of position data where the coordinate and the matching height is stored</param>
        public void MoveClusterUp(List<PosData> posData)
        {
            List<DataSet> dataSets = new List<DataSet>();
            foreach (PosData data in posData)
            {
                data.GetCoordinate().SetSphereX(-1);
                data.GetCoordinate().SetSphereY(-1);
                dataSets.Add(new DataSet(Command.SPHERE_UP, data));
            }
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Moves whole Kugelmatik up by a certain number
        /// </summary>
        /// <param name="posData">Defines how much the Spheres go up</param>
        public void MoveAllUp(short number)
        {
            List<DataSet> dataSets = new List<DataSet>();
            dataSets.Add(new DataSet(
                        Command.SPHERE_UP,
                        new PosData(Coordinate.WHOLE_KUGELMATIK, number)
                        ));
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Moves spheres down by a certain number
        /// </summary>
        /// <param name="posData">A list of position data where the coordinate and the matching height is stored</param>
        public void MoveDown(List<PosData> posData)
        {
            List<DataSet> dataSets = new List<DataSet>();
            foreach (PosData data in posData)
            {
                dataSets.Add(new DataSet(Command.SPHERE_DOWN, data));
            }
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Moves whole clusters down by a certain number
        /// </summary>
        /// <param name="posData">A list of position data where the coordinate and the matching height is stored</param>
        public void MoveClusterDown(List<PosData> posData)
        {
            List<DataSet> dataSets = new List<DataSet>();
            foreach (PosData data in posData)
            {
                data.GetCoordinate().SetSphereX(-1);
                data.GetCoordinate().SetSphereY(-1);
                dataSets.Add(new DataSet(Command.SPHERE_DOWN, data));
            }
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Moves whole Kugelmatik down by a certain number
        /// </summary>
        /// <param name="posData">Defines how much the Spheres go down</param>
        public void MoveAllDown(short number)
        {
            List<DataSet> dataSets = new List<DataSet>();
            dataSets.Add(new DataSet(
                        Command.SPHERE_DOWN,
                        new PosData(Coordinate.WHOLE_KUGELMATIK, number)
                        ));
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Sends specified spheres home.
        /// </summary>
        /// <param name="posData">A list of position data where the coordinate and the matching height is stored.</param>
        public void SendHome(List<PosData> posData)
        {
            List<DataSet> dataSets = new List<DataSet>();
            foreach (PosData data in posData)
            {
                dataSets.Add(new DataSet(Command.SEND_HOME, data));
            }
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Stops specified clusters.
        /// </summary>
        /// <param name="posData">A list of position data where the coordinate and the matching height is stored.</param>
        public void Stop(List<PosData> posData)
        {
            List<DataSet> dataSets = new List<DataSet>();
            foreach (PosData data in posData)
            {
                dataSets.Add(new DataSet(Command.STOP, data));
            }
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Fixes a certain Stepper
        /// </summary>
        /// <param name="posData">A list of position data where the coordinate and the matching height is stored.</param>
        public void Fix(List<PosData> posData)
        {
            List<DataSet> dataSets = new List<DataSet>();
            foreach (PosData data in posData)
            {
                dataSets.Add(new DataSet(Command.FIX, data));
            }
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Fixes every stepper in cluster
        /// </summary>
        /// <param name="posData">A list of position data where the coordinate and the matching height is stored.</param>
        public void FixCluster(List<PosData> posData)
        {
            List<DataSet> dataSets = new List<DataSet>();
            foreach (PosData data in posData)
            {
                data.GetCoordinate().SetSphereX(-1);
                data.GetCoordinate().SetSphereY(-1);
                dataSets.Add(new DataSet(Command.FIX, data));
            }
            client.SendPackage(new NetworkPackage(dataSets));
        }

        /// <summary>
        /// Fixes every stepper in the Kugelmatik
        /// </summary>
        public void MoveAllDown()
        {
            List<DataSet> dataSets = new List<DataSet>();
            dataSets.Add(new DataSet(
                        Command.FIX,
                        new PosData(Coordinate.WHOLE_KUGELMATIK)
                        ));
            client.SendPackage(new NetworkPackage(dataSets));
        }

        public Client GetClient()
        {
            return client;
        }
    }
}
