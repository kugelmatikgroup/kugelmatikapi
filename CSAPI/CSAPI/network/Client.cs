﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace CSAPI.network
{
    public class Client
    {
        private String host = "localhost";
        private int port = 4840;
        private int answerBufferSize = 256;

        // TODO use client.Connected; Safe?
        private bool isConnected = false;

        private TcpClient client;
        private NetworkStream stream;

        public Client(String host, int port)
        {
            this.host = host ?? throw new ArgumentNullException(nameof(host));
            this.port = port;
        }

        public Client(IPAddress addr, int port)
        {
            host = addr.ToString() ?? throw new ArgumentNullException(nameof(addr));
            this.port = port;
        }

        public Client()
        {
        }

        public void Connect()
        {
            if (!isConnected)
            {
                client = new TcpClient(host, port);
                stream = client.GetStream();
                isConnected = true;
            }
        }

        public void Disconnect()
        {
            if (isConnected)
            {
                stream.Close();
                client.Close();
                isConnected = false;
            }
        }

        public String SendData(Byte[] data)
        {
            if (!isConnected)
            {
                Connect();
            }
            stream.Write(data, 0, data.Length);
            data = new Byte[answerBufferSize];
            int responseBytes = stream.Read(data, 0, data.Length);
            return Encoding.ASCII.GetString(data, 0, responseBytes);
        }

        public String SendMessage(String msg)
        {
            return SendData(Encoding.ASCII.GetBytes(msg));
        }

        public String SendPackage(NetworkPackage package)
        {
            return SendMessage(package.ToJsonString());
        }

        public String GetHost()
        {
            return host;
        }

        public void SetHost(String host)
        {
            this.host = host ?? throw new ArgumentNullException(nameof(host));
        }

        public void SetHost(IPAddress addr)
        {
            host = addr.ToString() ?? throw new ArgumentNullException(nameof(addr));
        }

        public int GetPort()
        {
            return port;
        }

        public void SetPort(int port)
        {
            this.port = port;
        }

        public int GetAnswerBufferSize()
        {
            return answerBufferSize;
        }

        public void SetAnswerBufferSize(int newSize)
        {
            if (newSize <= 0)
                throw new ArgumentException(nameof(answerBufferSize) + " must be greater than 0.");
            answerBufferSize = newSize;
        }

        public bool IsConnected()
        {
            return isConnected;
        }

        public TcpClient GetCurrentClient()
        {
            return client;
        }

        public NetworkStream GetCurrentStream()
        {
            return stream;
        }
    }
}
