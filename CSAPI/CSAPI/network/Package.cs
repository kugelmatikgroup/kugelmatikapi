﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSAPI.network
{
    // TODO unnötig
    public class Package
    {
        private readonly Command cmd;
        private readonly List<Coordinate> coordinates;
        
        private short height = 0;

        public Package(Command cmd, List<Coordinate> coordinates)
        {
            this.cmd = cmd;
            this.coordinates = coordinates;
        }

        public Package(Command cmd, List<Coordinate> coordinates, short height)
        {
            this.cmd = cmd;
            this.coordinates = coordinates;
            this.height = height;
        }

        public String ToJsonString()
        {
            StringBuilder builder = new StringBuilder("{");
            builder.Append("\"cmd\":").Append((int)cmd).Append(", ");
            builder.Append("\"coordinates\":[");
            IEnumerator<Coordinate> enumerator = coordinates.GetEnumerator();
            bool hasNext;
            do
            {
                hasNext = enumerator.MoveNext();
                Coordinate coord = enumerator.Current;
                builder.Append(coord.ToJsonString());
                if (hasNext)
                {
                    builder.Append(", ");
                }
            } while (hasNext);
            builder.Append("], ");
            builder.Append("\"height\":").Append(height);
            builder.Append("}<EOP>");

            return builder.ToString();
        }        
    }
}
