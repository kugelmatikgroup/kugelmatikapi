﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSAPI.network
{
    public class DataSet
    {
        private Command cmd = Command.UNDEFINED;
        private Coordinate coordinate = Coordinate.WHOLE_KUGELMATIK;
        private short height = 0;

        public DataSet()
        {
        }

        public DataSet(PosData posData)
        {
            coordinate = posData.GetCoordinate();
            height = posData.GetHeightValue();
        }

        public DataSet(Command cmd, PosData posData)
        {
            this.cmd = cmd;
            coordinate = posData.GetCoordinate();
            height = posData.GetHeightValue();
        }

        public DataSet(Coordinate coordinate, short height)
        {
            this.coordinate = coordinate;
            this.height = height;
        }

        public DataSet(Coordinate coordinate, int height)
        {
            this.coordinate = coordinate;
            this.height = (short)height;
        }

        public DataSet(Command cmd, Coordinate coordinate, short height)
        {
            this.cmd = cmd;
            this.coordinate = coordinate;
            this.height = height;
        }

        public String ToJsonString()
        {
            return "{" +
                    "\"cmd\":" + (int)cmd + ", " +
                    "\"coordinate\":" + coordinate.ToJsonString() + ", " +
                    "\"heightValue\":" + height +
                    "}";
        }

        public Command getCmd()
        {
            return cmd;
        }

        public Coordinate getCoordinate()
        {
            return coordinate;
        }

        public short getHeight()
        {
            return height;
        }

        public void setCmd(Command cmd)
        {
            this.cmd = cmd;
        }

        public void setCoordinate(Coordinate coordinate)
        {
            this.coordinate = coordinate;
        }

        public void setHeight(short height)
        {
            this.height = height;
        }
    }
}
