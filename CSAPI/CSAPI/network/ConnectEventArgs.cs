﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSAPI.network
{
    public class ConnectEventArgs : EventArgs
    {
        private bool isConnected;
        private Exception error;

        public ConnectEventArgs(bool isConnected, Exception error)
        {
            this.isConnected = isConnected;
            this.error = error; 
        }

        public bool IsConnected { get { return isConnected; } }

        public Exception Error { get { return error; } }   
    }
}
