﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSAPI.network
{
    public enum Command : int
    {
        UNDEFINED = -1,
        SEND_HOME = 0,
        SPHERE_DOWN = 1,
        SPHERE_UP = 2,
        STOP = 3,
        SET = 4,
        FIX = 5
    }
}
