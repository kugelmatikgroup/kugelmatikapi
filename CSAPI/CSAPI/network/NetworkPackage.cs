﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSAPI.network
{
    public class NetworkPackage
    {
        public static readonly String clientInfo = "cs";

        private readonly List<DataSet> dataSets;

        public NetworkPackage(List<DataSet> dataSets)
        {
            this.dataSets = dataSets;
        }

        public String ToJsonString()
        {
            StringBuilder builder = new StringBuilder("{");
            builder.Append("\"dataSets\":[");
            for (int i = 0; i < dataSets.Count; i++)
            {
                DataSet data = dataSets[i];
                builder.Append(data.ToJsonString());
                if (i != dataSets.Count - 1)
                {
                    builder.Append(", ");
                }
            };
            builder.Append("], ");
            builder.Append("\"clientInfo\":\"").Append(clientInfo);
            builder.Append("\"}<EOP>");

            return builder.ToString();
        }

        public List<DataSet> GetDataSets()
        {
            return dataSets;
        }
    }
}
