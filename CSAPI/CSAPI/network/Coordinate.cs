﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSAPI.network
{
    public class Coordinate
    {
        public static Coordinate WHOLE_KUGELMATIK = new Coordinate();

        private int clusterX;
        private int clusterY;
        private int sphereX;
        private int sphereY;

        public Coordinate()
        {
            clusterX = -1;
            clusterY = -1;
            sphereX = -1;
            sphereY = -1;
        }

        public Coordinate(int clusterX, int clusterY)
        {
            this.clusterX = clusterX;
            this.clusterY = clusterY;
            sphereX = -1;
            sphereY = -1;
        }

        public Coordinate(int clusterX, int clusterY, int sphereX, int sphereY)
        {
            this.clusterX = clusterX;
            this.clusterY = clusterY;
            this.sphereX = sphereX;
            this.sphereY = sphereY;
        }

        public String ToJsonString()
        {
            return "[" +
                clusterX + ", " +
                clusterY + ", " +
                sphereX + ", " +
                sphereY +
                "]";
        }

        public int GetClusterX()
        {
            return clusterX;
        }

        public void SetClusterX(int clusterX)
        {
            this.clusterX = clusterX;
        }

        public int GetClusterY()
        {
            return clusterY;
        }

        public void SetClusterY(int clusterY)
        {
            this.clusterY = clusterY;
        }

        public int GetSphereX()
        {
            return sphereX;
        }

        public void SetSphereX(int sphereX)
        {
            this.sphereX = sphereX;
        }

        public int GetSphereY()
        {
            return sphereY;
        }

        public void SetSphereY(int sphereY)
        {
            this.sphereY = sphereY;
        }

    }
}
