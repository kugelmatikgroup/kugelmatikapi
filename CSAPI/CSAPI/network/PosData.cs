﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSAPI.network
{
    public class PosData
    {
        private readonly Coordinate coordinate;
        private readonly short heightValue = 0;

        public PosData(Coordinate coordinate)
        {
            this.coordinate = coordinate;
        }

        public PosData(Coordinate coordinate, short heightValue)
        {
            this.coordinate = coordinate;
            this.heightValue = heightValue;
        }

        public Coordinate GetCoordinate()
        {
            return coordinate;
        }

        public short GetHeightValue()
        {
            return heightValue;
        }
    }
}
